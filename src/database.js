const mongoose = require('mongoose')

const uri = "mongodb+srv://documental_user:documental_password@cluster0-z8mo6.mongodb.net/documental?retryWrites=true&w=majority";

const mongoseeOptions = {
    useFindAndModify: false,
    useNewUrlParser: true,
    useUnifiedTopology: true
};

mongoose.connect(uri, mongoseeOptions)
    .then(db => console.log('db is connected'))
    .catch(err => console.error(err))


module.exports = mongoose;
