import React, { Component } from 'react';

class App extends Component {

    componentDidMount() {
        this.fetchDocuments();
    }

    constructor() {
        super();
        this.state = {
            _id: '',
            documents: [],
            titulo: '',
            claves: [],
            descripcion: '',
            tipo_recurso: '',
            fuente: '',
            cobertura: {
                fecha_inicio: '',
                fecha_fin: '',
                latitude: 0,
                longitude: 0
            }
        }
        this.cobertura = {};


        this.agregarDocumento = this.agregarDocumento.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.initData();
        this.initChip([]);

    }

    initData() {
        document.addEventListener('DOMContentLoaded', () => {
            var elems = document.querySelectorAll('.datepicker');
            const options = {
                format: 'mmm dd, yyyy', onDraw: (e) => {
                    this.handleChange({ target: { name: e.el.name, value: e.date } });
                }
            };
            this.date_instance = M.Datepicker.init(elems, options);

        });
    }

    initChip(data) {
        document.addEventListener('DOMContentLoaded', () => {
            const elems = document.querySelectorAll('.chips');
            const options = {
                onChipAdd: (e) => {
                    this.handleChange({ target: { name: 'claves', value: (e[0].M_Chips.chipsData).map(claves => claves.tag) } });
                },
                onChipDelete: (e) => {
                    this.handleChange({ target: { name: 'claves', value: (e[0].M_Chips.chipsData).map(claves => claves.tag) } });
                }
            };
            this.chip = M.Chips.init(elems, options);
        });
    }

    handleChange(e) {
        const { name, value } = e.target;
        if (name === 'fecha_inicio' || name === 'fecha_fin') {
            this.cobertura = { ...this.cobertura, ...{ [name]: value } };
            this.setState({ cobertura: this.cobertura });
        } else if (name === 'tipo_recurso') {
            e.persist();
            this.setState({ [name]: value });
        } else {
            this.setState({ [name]: value });
        }

    }

    fetchDocuments() {
        fetch('api/document')
            .then(res => res.json())
            .then(data => {
                this.setState({ documents: data });
            });

    }

    agregarDocumento(e) {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(position => {
                const { latitude, longitude } = position.coords;
                this.cobertura = { ...this.cobertura, ...{ latitude: latitude, longitude: longitude } };
                this.setState({ cobertura: this.cobertura });
                this.saveDocument();
            });
        } else {
            this.saveDocument();
        }
        e.preventDefault();
    }

    saveDocument() {
        if (this.state._id !== '') {
            fetch(`/api/document/${this.state._id}`, {
                method: 'PUT',
                body: JSON.stringify(this.state),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
                .then(res => res.json())
                .then(data => {
                    M.toast({ html: 'Documento actualizado' });
                    this.clearForm();
                })
                .catch(err => console.log(err));
        } else {
            fetch('/api/document', {
                method: 'POST',
                body: JSON.stringify(this.state),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
                .then(res => res.json())
                .then(data => {
                    M.toast({ html: 'Documento creado' });
                    this.clearForm();

                })
                .catch(err => console.log(err));
        }
    }



    clearForm() {
        const lenArray = this.chip[0].chipsData.length;
        for (let i = 0; i < lenArray; i++) {
            this.chip[0].deleteChip(0);
        }
        this.chip[0].chipsData = [{ tag: 'asd' }];
        this.setState({
            _id: '',
            titulo: '',
            descripcion: '',
            tipo_recurso: '',
            claves: [],
            fuente: '',
            cobertura: {
                fecha_inicio: '',
                fecha_fin: '',
                latitude: 0,
                longitude: 0
            }
        });

        document.getElementById("formDocument").reset();
        this.initChip([]);
        this.fetchDocuments();
    }

    deleteDocument(id) {
        console.log("eliminando", id);
        fetch(`/api/document/${id}`, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then(res => res.json())
            .then(data => {
                M.toast({ html: 'Documento borrado' });
                this.fetchDocuments();
            })
    }

    setEditDocument(document) {
        const lenArray = this.chip[0].chipsData.length;
        for (let i = 0; i < lenArray; i++) {
            this.chip[0].deleteChip(0);
        }
        this.initChip(document.claves);
        document.claves.map(clave => (this.chip[0].addChip({ tag: clave })));
        this.setState(document);
    }

    render() {
        return (
            <div>
                {/* Navegación */}
                <nav className='light-blue darken-3'>
                    <div className='container'>
                        <a className='brand-logo' href='/' > MERN Comisión de la Verdad </a>
                    </div>
                </nav>
                <div className='container-inner'>
                    <div className='row'>
                        <div className='col s4'>
                            <div className='card'>
                                <div className='card-content'>
                                    <form onSubmit={this.agregarDocumento} id="formDocument">
                                        <div className='row'>
                                            <div className='input-field col s12'>
                                                <input onChange={this.handleChange} name='titulo' type='text' placeholder='Título del documento' value={this.state.titulo} />
                                                <textarea onChange={this.handleChange} name='descripcion' className='materialize-textarea' placeholder='Descripción' value={this.state.descripcion}></textarea>
                                                <div className='chips'>
                                                    <input name='claves' className='custom-class' placeholder='Claves , Enter' />
                                                </div>
                                                <input onChange={this.handleChange} name='fuente' type='text' placeholder='Fuente' value={this.state.fuente} />
                                                <div className="row">
                                                    <label>Tipo del Recurso:</label>
                                                    <select name="tipo_recurso" onChange={this.handleChange} className='browser-default' defaultValue={this.state.tipo_recurso}>
                                                        <option value=''>Seleccione el tipo de recurso: ...</option>
                                                        <option value='Testimonio'>Testimonio</option>
                                                        <option value='Informe'>Informe</option>
                                                        <option value='Caso'>Caso</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <input name='fecha_inicio' type='text' className='datepicker' placeholder='Fecha inicio' />
                                            <input name='fecha_fin' type='text' className='datepicker' placeholder='Fecha fin' />
                                            <div align='center'>
                                                <button type='submit' className='btn btn-light light-blue lighten-1'>
                                                    Guardar
                                                    </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div className='col s8'>
                            <div className='card'>
                                <div className='card-content'>
                                    <table>
                                        <thead>
                                            <tr>
                                                <th>Titulo</th>
                                                <th>Descripcion</th>
                                                <th>Claves</th>
                                                <th>Tipo Recurso</th>
                                                <th>Fuente</th>
                                                <th>Localizacion</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                this.state.documents.map(document => {
                                                    return (
                                                        <tr key={document._id}>
                                                            <td>{document.titulo}</td>
                                                            <td>{document.descripcion}</td>
                                                            <td>{document.claves.toString()}</td>
                                                            <td>{document.tipo_recurso}</td>
                                                            <td>{document.fuente}</td>
                                                            <td>
                                                                <a target='_blank'
                                                                    href={'https://www.google.com/maps/search/?api=1&query=' + document.cobertura.latitude + ',' + + document.cobertura.longitude}>
                                                                    localización
                                                                    </a>

                                                            </td>
                                                            <td>
                                                                <a className="btn-floating btn-small btn-large waves-effect waves-light red" onClick={() => this.deleteDocument(document._id)}>
                                                                    <i className="material-icons">delete</i>
                                                                </a>
                                                                <a className="btn-floating btn-small btn-large waves-effect waves-light light-blue lighten-1" onClick={() => this.setEditDocument(document)}>
                                                                    <i className="material-icons">edit</i>
                                                                </a>
                                                            </td>

                                                        </tr>
                                                    )
                                                })
                                            }
                                        </tbody>
                                    </table>
                                    <div align='center'>
                                        <button className='btn btn-light light-blue lighten-1' onClick={() => this.clearForm()}>Nuevo</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

export default App;