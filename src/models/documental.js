const mongoose = require('mongoose')
const { Schema } = mongoose

const DocumentSchema = new Schema({
    titulo: { type: String, required: true },
    claves: [String],
    descripcion: String,
    tipo_recurso: String,
    fuente: String,
    cobertura: {
        fecha_inicio: Date,
        fecha_fin: Date,
        latitude: Number,
        longitude: Number,
    }
});

module.exports = mongoose.model('Documento', DocumentSchema)