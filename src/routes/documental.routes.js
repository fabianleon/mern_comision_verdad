const express = require('express');
const router = express.Router();

// Object mongosee
const Documental = require('../models/documental')

const castDocument = ({ titulo, claves, descripcion, fuente, cobertura, tipo_recurso }) => {
    return { titulo, claves, descripcion, fuente, cobertura, tipo_recurso }
}

// get list documents
router.get('/api/document', async (req, res) => {
    const documentos = await Documental.find();
    res.json(documentos);
});

// get list documents
router.get('/api/document/:id', async (req, res) => {
    const documento = await Documental.findById(req.params.id);
    res.json(documento);
});

// post document function
router.post('/api/document', async (req, res) => {
    const doc = castDocument(req.body);
    const documento = new Documental(doc);
    await documento.save()
    res.json({ status: 'document saved' });
});

// put document function
router.put('/api/document/:id', async (req, res) => {
    const doc = castDocument(req.body);
    await Documental.findByIdAndUpdate(req.params.id, doc);

    res.json({ status: 'document updated' })

});
// delete document function
router.delete('/api/document/:id', async (req, res) => {
    await Documental.findByIdAndDelete(req.params.id);
    res.json({ status: 'deleted document' })

});

module.exports = router;